package com.example.javad.kashi_tamrin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_Aboutus = (Button) findViewById(R.id.button_aboutus);
        Button btn_Buy = (Button) findViewById(R.id.button_Buy);
        Button btn_Signup = (Button) findViewById(R.id.button_Signup);
        Button btn_Login = (Button) findViewById(R.id.button_Login);

        btn_Aboutus.setOnClickListener(this);
        btn_Buy.setOnClickListener(this);
        btn_Login.setOnClickListener(this);
        btn_Signup.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        Intent intent_about = new Intent(MainActivity.this, boutUs.class);
        Intent intent_Login = new Intent(MainActivity.this, Login.class);
        Intent intent_SignUp = new Intent(MainActivity.this, SignUp.class);
        Intent intent_Buy = new Intent(MainActivity.this, Buy.class);

        switch (v.getId()) {
            case R.id.button_aboutus:
                startActivity(intent_about);
                break;
            case R.id.button_Buy:
                startActivity(intent_Buy);
                break;
            case R.id.button_Login:
                startActivity(intent_Login);
                break;
            case R.id.button_Signup:
                startActivity(intent_SignUp);
                break;
        }
    }
}